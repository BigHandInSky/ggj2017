﻿using System.Collections;
using UnityEngine;

namespace Utilities.GuiSchwp
{
    public abstract class SchwpAction : ScriptableObject
    {
        public abstract IEnumerator PeformActionForw (Schwpper caller);
        public abstract IEnumerator PeformActionBack (Schwpper caller);
    }
}
