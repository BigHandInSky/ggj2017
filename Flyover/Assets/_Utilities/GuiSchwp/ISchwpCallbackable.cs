﻿using UnityEngine;

namespace Utilities.GuiSchwp
{
    public interface ISchwpCallbackable
    {
        void Callback (MonoBehaviour caller);
    }
}
