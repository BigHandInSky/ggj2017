﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities.GuiSchwp
{
    /// <summary>
    /// Basic form of Gui Schwp, that can be controlled directly/SchwpGroup, and extended
    /// </summary>
    public class Schwpper : MonoBehaviour
    {
        [Header("Actions")]
        public SchwpActions.SetTransform            SetValues;
        public SchwpAction[]                        Actions = new SchwpAction[0];

        [Header("Unity Event Triggers")]
        [SerializeField] private bool               m_SchwpOnStart = false;
        [SerializeField] private bool               m_SchwpOnEnable = false;
        [SerializeField] private bool               m_Forwards = true;
        [SerializeField]
        private ISchwpCallbackable                  m_OnStartOrEnableCaller = null;

        [Header("Base Settings")]
        [SerializeField] protected RectTransform    m_Transf;
        protected float                             m_ActionInterval = 0.1f;

        protected ISchwpCallbackable                m_Caller;

        public RectTransform RectTransf { get { return m_Transf; } }

        void Start()
        {
            SetValues.PeformActionForw(this);

            if (m_SchwpOnStart)
            {
                Trigger(m_Forwards, m_OnStartOrEnableCaller);
            }
        }
        void OnEnable ()
        {
            if (m_SchwpOnEnable)
            {
                Trigger(m_Forwards, m_OnStartOrEnableCaller);
            }
        }

        public void Trigger (bool forward)
        {
            Prepare(forward);

            if (forward)
            {
                StartCoroutine(ActionForward());
            }
            else
            {
                StartCoroutine(ActionBackward());
            }
        }
        public void Trigger (bool forward, ISchwpCallbackable caller)
        {
            m_Caller = caller;
            Trigger(forward);
        }

        protected virtual void Prepare (bool forward)
        {
            if (forward)
            {
                StartCoroutine(SetValues.PeformActionForw(this));
            }
            else
            {
                StartCoroutine(SetValues.PeformActionBack(this));
            }
        }

        protected virtual IEnumerator ActionForward ()
        {
            WaitForSeconds waitFor = new WaitForSeconds(m_ActionInterval);

            int actionCount = Actions.Length;
            for(int loop = 0; loop < actionCount; loop++)
            {
                yield return StartCoroutine(Actions[loop].PeformActionForw(this));
                yield return waitFor;
            }

            Complete();
        }
        protected virtual IEnumerator ActionBackward ()
        {
            WaitForSeconds waitFor = new WaitForSeconds(m_ActionInterval);

            int actionCount = Actions.Length;
            for (int loop = actionCount - 1; loop > -1; loop--)
            {
                yield return StartCoroutine(Actions[loop].PeformActionBack(this));
                yield return waitFor;
            }

            Complete();
        }

        protected void Complete ()
        {
            if (m_Caller != null)
            {
                m_Caller.Callback(this);
                m_Caller = null;
            }
        }
    }
}