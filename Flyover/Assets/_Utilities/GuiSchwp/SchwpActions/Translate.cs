﻿using System.Collections;
using UnityEngine;

namespace Utilities.GuiSchwp.SchwpActions
{
    [CreateAssetMenu(fileName = "SchwpTranslate_", menuName = "Schwp Actions/Translate")]
    public class Translate : SchwpAction
    {
        [Header("Variables")]
        [SerializeField] private float m_XChange = 0.0f;
        [SerializeField] private float m_YChange = 10.0f;
        [SerializeField] private float m_Length = 1.0f;

        [Header("Options")]
        [SerializeField] private bool m_PreCalculatedLerp = true;

        public override IEnumerator PeformActionForw (Schwpper caller)
        {
            Vector2 start = caller.RectTransf.anchoredPosition;
            Vector2 end = start;
            end.x += m_XChange;
            end.y += m_YChange;

            float lerp = 0;
            while(lerp < 1)
            {
                yield return null;
                lerp += Time.deltaTime / m_Length;

                if (m_PreCalculatedLerp)
                {
                    caller.RectTransf.anchoredPosition = Vector2.Lerp(start, end, lerp);
                }
                else
                {
                    caller.RectTransf.anchoredPosition = Vector2.Lerp(caller.RectTransf.anchoredPosition, end, lerp);
                }
            }
        }
        public override IEnumerator PeformActionBack (Schwpper caller)
        {
            Vector2 start = caller.RectTransf.anchoredPosition;
            Vector2 end = start;
            end.x -= m_XChange;
            end.y -= m_YChange;

            float lerp = 0;
            while (lerp < 1)
            {
                yield return null;
                lerp += Time.deltaTime / m_Length;

                if (m_PreCalculatedLerp)
                {
                    caller.RectTransf.anchoredPosition = Vector2.Lerp(start, end, lerp);
                }
                else
                {
                    caller.RectTransf.anchoredPosition = Vector2.Lerp(caller.RectTransf.anchoredPosition, end, lerp);
                }
            }
        }
    }
}
