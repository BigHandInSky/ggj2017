﻿using System.Collections;
using UnityEngine;

namespace Utilities.GuiSchwp.SchwpActions
{
    [CreateAssetMenu(fileName = "SchwpSetTransform_", menuName = "Schwp Actions/Set Transform")]
    public class SetTransform : SchwpAction
    {
        [Header("Forward Set")]
        [SerializeField] private Vector2        m_FAnchoredPos;
        [SerializeField] private Vector3        m_FScale = Vector3.one;
        [SerializeField] private Quaternion     m_FRotation;

        [Header("Backward Set")]
        [SerializeField] private Vector2        m_BAnchoredPos;
        [SerializeField] private Vector3        m_BScale = Vector3.one;
        [SerializeField] private Quaternion     m_BRotation;

        [Header("Options")]
        [SerializeField] private bool           m_SetAfterAFrame = true;

        public override IEnumerator PeformActionForw (Schwpper caller)
        {
            if (!m_SetAfterAFrame)
                SetValues(true, caller.RectTransf);

            yield return null;

            if (m_SetAfterAFrame)
                SetValues(true, caller.RectTransf);
        }
        public override IEnumerator PeformActionBack (Schwpper caller)
        {
            if (!m_SetAfterAFrame)
                SetValues(false, caller.RectTransf);

            yield return null;

            if (m_SetAfterAFrame)
                SetValues(false, caller.RectTransf);
        }

        private void SetValues(bool forwards, RectTransform transf)
        {
            transf.anchoredPosition = ( forwards ) ? m_FAnchoredPos : m_BAnchoredPos;
            transf.localRotation = ( forwards ) ? m_FRotation : m_BRotation;
            transf.localScale = ( forwards ) ? m_FScale : m_BScale;
        }
    }
}
