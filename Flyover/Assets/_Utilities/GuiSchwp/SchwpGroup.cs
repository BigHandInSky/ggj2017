﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities.GuiSchwp
{
    public class SchwpGroup : MonoBehaviour, ISchwpCallbackable
    {
        [SerializeField] SchwpGroup[]               m_Groups = new SchwpGroup[0];
        [SerializeField] Schwpper[]                 m_Schwppers = new Schwpper[0];

        protected int                               m_TotalExpectedCallbacks = -1;
        protected int                               m_Callbacks = -1;
        protected ISchwpCallbackable                m_Caller;

        public void TriggerSchwps (bool forward, float initialDelay = 0)
        {
            StartCoroutine(Trigger(forward, initialDelay));
        }
        public void TriggerSchwps (ISchwpCallbackable caller, bool forward, float initialDelay = 0)
        {
            m_Caller = caller;
            TriggerSchwps(forward, initialDelay);
        }

        protected virtual IEnumerator Trigger(bool forward, float initialDelay)
        {
            m_TotalExpectedCallbacks = 0;
            m_Callbacks = 0;

            float waitfor = initialDelay;
            while(waitfor > 0)
            {
                waitfor -= Time.deltaTime;
                yield return null;
            }


            int actionCount = m_Groups.Length;
            m_TotalExpectedCallbacks += actionCount;
            for (int loop = 0; loop < actionCount; loop++)
            {
                m_Groups[loop].TriggerSchwps(forward);
                yield return null;
            }

            actionCount = m_Schwppers.Length;
            m_TotalExpectedCallbacks += actionCount;
            for (int loop = 0; loop < actionCount; loop++)
            {
                m_Schwppers[loop].Trigger(forward);
                yield return null;
            }
        }

        public void Callback (MonoBehaviour caller)
        {
            m_Callbacks++;

            if(m_Callbacks == m_TotalExpectedCallbacks)
            {
                if (m_Caller != null)
                {
                    m_Caller.Callback(this);
                    m_Caller = null;
                }
            }
            else if(m_Callbacks == m_TotalExpectedCallbacks)
            {
                Debug.LogWarning("SchwpGroup[" + name + "]: Callback(" + caller.name + "): has gotten more callbacks[" + m_Callbacks + "] than expected[" + m_TotalExpectedCallbacks + "] ");
            }
        }
    }
}