﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GGJ17.Abjects;

namespace GGJ17.World
{
    public class WorldManager : MonoBehaviour
    {
        private static WorldManager m_Instance;
        public static WorldManager Instance
        {
            get
            {
                if (!m_Instance)
                    m_Instance = FindObjectOfType<WorldManager>();

                return m_Instance;
            }
        }

        [Header("Abjects")]
        [SerializeField] private Transform      m_AbjectRoot;
        [SerializeField] private GameObject[]   m_AbjectFabs = new GameObject[0];
        
        [Header("Platform & Obstacles")]
        [SerializeField] private Transform      m_PlatformsRoot;
        public GameObject                       PlatformFab;
        public GameObject[]                     ObstacleFabs = new GameObject[0];

        [Header("Raycast")]
        [SerializeField] private LayerMask      m_PlatformMasks;

        private List<GameObject>                m_PlatformSpawns = new List<GameObject>(0);
        private List<GameObject>                m_AbjectSpawns = new List<GameObject>(0);

        private int                             m_CylindersSpawned = 0;

        private Vector3                         m_TestRayStartAdd = new Vector3(0, 0.1f, 0);

        private Vector3                         m_DistanceUnit = new Vector3(0.0f, 0.0f, 20.0f);
        private Vector3                         m_LastEuler = Vector3.zero;
        private float                           m_Modifier;

        private const float     SPAWN_TIME_MOD = 1.0f;
        private const float     MIN_SPAWN_TIME = 1.0f;
        private const float     MAX_SPAWN_TIME = 3.0F;

        private const float     MIN_RAND_EULER = 25.0F;
        private const float     MAX_RAND_EULER = 90.0F;

        private const int       MIN_PLATS_FOR_CYL = 3;
        private const int       MIN_REPEAT_SPAWNS = 2;

        private const int       MAX_ABJECTS = 20;

        public const float      PLATF_BASE_ROT_SPEED = 15.0F;
        public const float      PLATF_ROOT_ROT_SPEED = 3.0F;

        void Start()
        {
            Common.Restart();
            StartCoroutine(AbjectSpawner());
        }
        
        void Update()
        {
            m_PlatformsRoot.Rotate(Vector3.up, PLATF_ROOT_ROT_SPEED * Time.deltaTime);
        }

        public void EndOfLife ()
        {
            StopAllCoroutines();
            m_CylindersSpawned = 0;

            for (int loop = 0; loop < m_AbjectSpawns.Count; loop++)
            {
                m_AbjectSpawns[loop].SendMessage("EndOfLifeKill", SendMessageOptions.RequireReceiver);
            }
            m_AbjectSpawns.Clear();
        }

        public void CheckPlayerDeathPosition(Vector3[] positions, Abjects.AbjectType killType)
        {
            // check for platform
            RaycastHit hitInfo;
            for(int loop = 0; loop < positions.Length; loop++)
            {
                if (Physics.Raycast(
                    positions[loop] + m_TestRayStartAdd,
                    Vector3.down, 
                    out hitInfo, 
                    3.0f, 
                    m_PlatformMasks))
                {/*
                    // add an obstacle
                    //Debug.Log("WorldManager: CheckPlayerDeathPosition: found collider: " + hitInfo.collider.name, hitInfo.collider);

                    PlatformSpread spreader = hitInfo.collider.GetComponent<PlatformSpread>();
                    Platform platform = hitInfo.collider.GetComponent<Platform>();

                    if (platform)
                        platform.SpawnObstacle(ObstacleFabs[(int)killType]);
                    else if(spreader)
                        spreader.ParentPlatform.SpawnObstacle(ObstacleFabs[(int)killType]);
                    //else
                        //Debug.Log("WorldManager: CheckPlayerDeathPosition: found a collider but no platform/spreader under it");
            */
                }
                else
                {
                    //Debug.Log("WorldManager: CheckPlayerDeathPosition: did not find collider");
                    // spawn platform
                    SpawnPlatform(positions[loop]);
                }
            }
        }
        private void SpawnPlatform(Vector3 position)
        {
            GameObject platformClone = Instantiate(PlatformFab);
            platformClone.transform.SetParent(m_PlatformsRoot);
            platformClone.transform.position = position;
            
            m_PlatformSpawns.Add(platformClone);
            platformClone.name += "_" + m_PlatformSpawns.Count;
        }

        public void NewLife()
        {
            StopAllCoroutines();
            StartCoroutine(AbjectSpawner());
        }

        public void Restart()
        {
            StopAllCoroutines();

            for(int loop = 0; loop < m_PlatformSpawns.Count; loop++)
            {
                Destroy(m_PlatformSpawns[loop]);
            }
            for (int loop = 0; loop < m_AbjectSpawns.Count; loop++)
            {
                m_AbjectSpawns[loop].SendMessage("EndOfLifeKill", SendMessageOptions.RequireReceiver);
            }

            NewLife();
        }
        
        private IEnumerator AbjectSpawner()
        {
            m_Modifier = SPAWN_TIME_MOD * (Common.LivesLeft + 1);
            float wait = MAX_SPAWN_TIME;

            AbjectType lastType = AbjectType.Cylinder;
            int repeats = 0;

            bool pigsFlying = false;
            while(!pigsFlying)
            {
                yield return new WaitForSeconds(wait);

                while (m_AbjectSpawns.Count >= MAX_ABJECTS)
                {
                    Debug.Log("WorldManager: AbjectSpawner: max abject limit reached");
                    yield break;
                }

                m_LastEuler.y += Random.Range(MIN_RAND_EULER, MAX_RAND_EULER);
                Vector3 spawnPos = Quaternion.Euler(m_LastEuler) * m_DistanceUnit;

                int max = (m_PlatformSpawns.Count < (MIN_PLATS_FOR_CYL * (m_CylindersSpawned + 1))) ? 3 : 4;
                AbjectType spawnType = (AbjectType)Random.Range(0, max);

                if(spawnType == lastType)
                {
                    repeats++;
                    if(repeats >= MIN_REPEAT_SPAWNS)
                    {
                        int intType = (int)spawnType + 1;
                        if (intType >= max)
                            intType = 0;

                        spawnType = (AbjectType)intType;
                        Debug.Log("repeat clamp triggered");
                    }
                }

                if (spawnType == AbjectType.Cylinder)
                    m_CylindersSpawned++;

                lastType = spawnType;

                GameObject abjectClone = Instantiate(m_AbjectFabs[(int)spawnType]);

                abjectClone.transform.SetParent(m_AbjectRoot);
                abjectClone.transform.localPosition = spawnPos;

                abjectClone.GetComponent<Abject>().StartingVectors(m_DistanceUnit, m_LastEuler);
            
                m_AbjectSpawns.Add(abjectClone);
                
                wait = Random.Range(
                        MIN_SPAWN_TIME * m_Modifier,
                        MAX_SPAWN_TIME * m_Modifier);
            }
        }
    }
}
