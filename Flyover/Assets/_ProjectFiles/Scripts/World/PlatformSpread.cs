﻿using UnityEngine;

namespace GGJ17.World
{
    public class PlatformSpread : MonoBehaviour
    {
        private Transform                   m_Self;
        private Platform                    m_Parent;

        private float                       m_SineTime = 0;
        private float                       m_SinePow = 1.5f;
        private float                       m_MinRadius = 1.5f;
        /*
        [SerializeField] private float      m_EulerRotSpeed = 15.0f;
        private Vector3                     m_EulerRot = Vector3.zero;
        */
        public Platform ParentPlatform { get { return m_Parent; } }

        void Start()
        {
            m_Self = transform;
            m_Parent = GetComponentInParent<Platform>();
        }

        void Update()
        {
            m_SineTime += Time.deltaTime;
            float sineVal = m_MinRadius + (Mathf.Sin(m_SineTime) * m_SinePow);
            m_Self.localScale = new Vector3(sineVal, 1, sineVal);
        }
        
        /*
        void Update()
        {
            m_EulerRot.y += Time.deltaTime * m_EulerRotSpeed;
            m_Self.localEulerAngles = m_EulerRot;
        }*/
    }
}