﻿using UnityEngine;

namespace GGJ17.World
{
    public class Platform : MonoBehaviour
    {
        private Transform                           m_Self;

        [SerializeField] private bool               m_IsStartingPlatform = false;
        [SerializeField] private PlatformSpread     m_Spread;
        [SerializeField] private Transform          m_ObstaclePoint;

        private float                               m_EulerRotSpeed = 0.0f;
        private float                               m_MinSpeedRange = -5.0f;
        private float                               m_MaxSpeedRange = +25.0f;
        private Vector3                             m_EulerRot = Vector3.zero;

        private bool                                m_HasCylinder = false;

        private Obstacle                            m_Obstacle;

        public bool         IsStartingPlatform { get { return m_IsStartingPlatform; } }
        public Vector3      WorldPos { get { return m_Self.position; } }

        public bool         HasCylinder { get { return m_HasCylinder; } set { m_HasCylinder = value; } }

        private void Start()
        {
            m_Self = transform;
            m_HasCylinder = false;

            if (!m_IsStartingPlatform)
            {
                m_EulerRotSpeed = Random.Range(m_MinSpeedRange, m_MaxSpeedRange);
                StartCoroutine(StartScale());
            }
            else
            {
                m_EulerRotSpeed = WorldManager.PLATF_BASE_ROT_SPEED;
            }
        }

        System.Collections.IEnumerator StartScale()
        {
            float lerp = 0.0f;
            while(lerp < 1.0f)
            {
                yield return null;

                lerp += Time.deltaTime / 0.75f;
                m_Self.localScale = Vector3.one * lerp;
            }

            m_Self.localScale = Vector3.one;
        }

        public void SpawnObstacle(GameObject obstacleFabType)
        {
            if (m_IsStartingPlatform)
                return;

            if (m_Obstacle != null)
                return;

            GameObject clone = Instantiate(obstacleFabType);
            clone.transform.SetParent(m_ObstaclePoint);

            m_Obstacle = clone.GetComponent<Obstacle>();
            m_Obstacle.Setup();
        }

        void Update()
        {
            m_EulerRot.y += Time.deltaTime * m_EulerRotSpeed;
            m_Self.localEulerAngles = m_EulerRot;
        }
    }
}