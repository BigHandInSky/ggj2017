﻿using UnityEngine;

namespace GGJ17.World
{
    [RequireComponent(typeof(Collider))]
    public class Obstacle : MonoBehaviour
    {
        private float m_Health = 1.0f;

        public void Setup()
        {
            transform.localPosition = Vector3.zero;
            //transform.localScale = Vector3.one;
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag(Common.TAG_PLAYER))
            {
                // heal
                m_Health += Time.deltaTime;
                m_Health = Mathf.Clamp01(m_Health);
            }
            else if (collision.gameObject.CompareTag(Common.TAG_ABJECT))
            {
                Debug.Log("Obstacle[" + name + "]: OnCollisionEnter(" + collision + "): hit abject");

                m_Health -= Time.deltaTime;
                if (m_Health <= 0.0f)
                {
                    Destroyed();
                }
            }
        }
        private void Destroyed()
        {
            Destroy(gameObject);
        }
    }
}
