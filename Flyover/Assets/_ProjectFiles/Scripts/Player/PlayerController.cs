﻿using UnityEngine;
using GGJ17.Abjects;

namespace GGJ17.Player
{
    [RequireComponent(typeof(Collider))]
    public class PlayerController : MonoBehaviour
    {
        private static PlayerController m_Instance;
        public static PlayerController Instance
        {
            get
            {
                if (!m_Instance)
                    m_Instance = FindObjectOfType<PlayerController>();

                return m_Instance;
            }
        }

        [SerializeField] private PlayerMover    m_Mover;
        [SerializeField] private AudioSource    m_DeathAudio;
        [SerializeField] private Transform[]    m_SpawnPositions = new Transform[0];
        private bool                            m_Alive = true;
        /*
        [SerializeField] private bool DebugKill = false;
        [SerializeField] private AbjectType KillType = AbjectType.Square;*/

        public void Restart()
        {
            m_Alive = true;
            PlayerMover.CanControl = true;

            m_Mover.Restart();
        }

        void Update()
        {/*
            if(DebugKill)
            {
                DebugKill = false;
                Destroyed(KillType);
            }*/

            if(m_Alive)
            {
                Common.IncrementScore();
            }
        }

        public void OnCollisionEnter(Collision collision)
        {
            GameObject collided = collision.gameObject;
            if (collided.CompareTag(Common.TAG_ABJECT))
            {
                //Debug.Log("Player: OnCollisionEnter(" + collision + "): hit abject");
                Destroyed(collided.GetComponent<Abject>().GetAbjectType);
            }
        }
        private void Destroyed(AbjectType collType)
        {
            m_Alive = false;
            PlayerMover.CanControl = false;
            m_DeathAudio.Play();

            if (Common.LivesLeft < 1)
            {
                Common.EndOfLife();
                Gui.Game.GuiManager.Instance.EndOfGame();
            }
            else
            {
                Common.EndOfLife();
                Gui.Game.GuiManager.Instance.EndOfLife();

                Vector3[] positions = new Vector3[m_SpawnPositions.Length];
                for(int loop = 0; loop < m_SpawnPositions.Length; loop++)
                {
                    positions[loop] = m_SpawnPositions[loop].position;
                }

                World.WorldManager.Instance.CheckPlayerDeathPosition(positions, collType);
            }

            World.WorldManager.Instance.EndOfLife();
        }
    }
}
