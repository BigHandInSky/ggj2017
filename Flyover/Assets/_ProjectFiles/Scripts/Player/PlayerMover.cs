﻿using UnityEngine;

namespace GGJ17.Player
{
    public sealed class PlayerMover : MonoBehaviour
    {
        private Transform                   m_Self;

        private float                       m_EulerRotSpeed = 15.0f;
        private Vector3                     m_EulerRot = Vector3.zero;

        [Header("Speed modifiers")]
        [SerializeField] private float      m_TestSpeedMod = 1.0f;
        [SerializeField] private float      m_NormSpeedMod = 1.0f;
        [SerializeField] private float      m_PlatfSpeedMod = 1.0f;

        [Header("Raycast")]
        [SerializeField] private LayerMask m_PlatformMasks;

        private ControlType m_Controls;

        public static bool                  CanControl = true;
        
        private Vector3 m_TestRayStartAdd = new Vector3(0, 0.1f, 0);
        public static Vector3 PlayerPosition { get; private set; }

        private void Start()
        {
            m_Self = transform;
            m_Controls = Common.ControlMethod;
        }

        public void Restart()
        {
            m_Self.localPosition = Vector3.zero;
        }

        private void Update()
        {
            PlayerPosition = m_Self.position;

            m_EulerRot.y += Time.deltaTime * m_EulerRotSpeed;
            m_Self.localEulerAngles = m_EulerRot;

            if (!CanControl)
                return;

            Vector3 totalDir = Vector3.zero;
            if (m_Controls == ControlType.WASD)
            {
                if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                    totalDir += Vector3.forward;

                if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                    totalDir += Vector3.back;

                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                    totalDir += Vector3.left;

                if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                    totalDir += Vector3.right;
            }
            else
            {
                Vector3 mousePos = Input.mousePosition;
                Vector3 screenHalf = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0);
                Vector3 normalized = Vector3.Normalize(mousePos - screenHalf);

                totalDir = new Vector3(normalized.x, 0, normalized.y);
            }

            if (totalDir != Vector3.zero)
                Move(totalDir);
        }

        public void Move(Vector3 direction)
        {         
            if (TestLocation(m_Self.localPosition + direction * m_TestSpeedMod))
            {
                m_Self.localPosition = m_Self.localPosition + direction * (m_PlatfSpeedMod * Time.deltaTime);                
            }
            else
            {
                m_Self.localPosition = m_Self.localPosition + direction * (m_NormSpeedMod * Time.deltaTime);
                Common.IncrementMultiplier();
            }
        }

        private bool TestLocation(Vector3 testPos)
        {
            Debug.DrawRay(testPos + m_TestRayStartAdd, Vector3.down, Color.green, 0.5f);
            return Physics.Raycast(testPos + m_TestRayStartAdd, Vector3.down, m_PlatformMasks);
        }
    }

    public enum ControlType
    {
        WASD,
        Mouse
    }
}
