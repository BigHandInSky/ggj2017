﻿using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.DynamicMenu
{
    /// <summary>
    /// Basic button for changing menu roots
    /// </summary>
    public class RootChangerButton : MonoBehaviour, IRootCallable
    {
        [SerializeField] private Button     m_ButtonComponent;
        [SerializeField] private MenuRoot   m_ChangeToRoot;
        private MenuRoot                    m_ParentRoot;

        public void RootOpen (MenuRoot caller)
        {
            if (!m_ChangeToRoot)
                Debug.LogWarning("RootChangerButton[" + name + "]: RootOpen: no ChangeToRoot assigned", gameObject);
            if (!m_ButtonComponent)
                Debug.LogWarning("RootChangerButton[" + name + "]: RootOpen: no Button Component assigned", gameObject);

            m_ParentRoot = caller;
            m_ButtonComponent.onClick.AddListener(Clicked);
        }

        private void Clicked()
        {
            m_ParentRoot.OpenOtherRoot(m_ChangeToRoot);
        }

        public void RootClose ()
        {
            // do nothing
        }
    }
}
