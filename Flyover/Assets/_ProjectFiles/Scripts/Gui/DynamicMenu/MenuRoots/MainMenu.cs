using UnityEngine;

namespace GGJ17.Gui.DynamicMenu
{
    /// <summary>
    /// Menu Root which primarily starts up the game, or other roots
    /// </summary>
    public sealed class MainMenu : MenuRoot
    {
        [Header("Roots")]
        [SerializeField] private MenuRoot m_GameStart;
        [SerializeField] private MenuRoot m_Options;
        [SerializeField] private MenuRoot m_Credits;

        protected override void InheritedStart ()
        {
            OpenRoot();
        }

        public void OpenGame ()
        {
            OpenOtherRoot(m_GameStart);
        }
        public void OpenOptions ()
        {
            OpenOtherRoot(m_Options);
        }
        public void OpenCredits ()
        {
            OpenOtherRoot(m_Credits);
        }
        public void ExitApp ()
        {
            Application.Quit();
        }
    }
}