﻿using UnityEngine;

namespace GGJ17.Gui.DynamicMenu
{
    /// <summary>
    /// Interface for objects that can be created by a GuiLister dynamically
    /// </summary>
    public interface IGuiListable
    {        
        void Setup (Callables.GuiLister lister, string text, int type);
    }

    [System.Serializable]
    public struct ListableData
    {
        public GameObject   Prefab;
        public string       MainText;
        [Range(0,100)]
        public int          BasicType;
    }
}
