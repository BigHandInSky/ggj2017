﻿
namespace GGJ17.Gui.DynamicMenu
{
    /// <summary>
    /// Basic interface for objects that can be called by a MenuRoot without a direct reference
    /// </summary>
    public interface IRootCallable
    {
        // When the root is opened, as an alternative to Start/OnEnable
        void RootOpen (MenuRoot caller);
        // When the root is closed, as an alternative to OnDisable
        void RootClose ();
    }
}
