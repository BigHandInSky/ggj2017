using System;
using UnityEngine;

namespace GGJ17.Gui.DynamicMenu.Callables
{
    /// <summary>
    /// Class which handles spawning in a given set of IGuiListables
    /// </summary>
    public class GuiLister : MonoBehaviour, IRootCallable
    {
        [SerializeField] protected RectTransform    m_SpawnRoot;
        [SerializeField] protected ListableData[]   m_Listables;

        protected IGuiListable[]                    m_Spawned;

        public virtual void RootOpen (MenuRoot caller)
        {
            if(m_Spawned == null || m_Spawned.Length != m_Listables.Length)
            {
                int spawnCount = m_Listables.Length;
                m_Spawned = new IGuiListable[spawnCount];

                for (int loop = 0; loop < spawnCount; loop++)
                {
                    GameObject spawned = Instantiate(m_Listables[loop].Prefab);
                    Transform spawnTransf = spawned.transform;

                    spawnTransf.SetParent(m_SpawnRoot);
                    spawnTransf.localScale = Vector3.one;

                    m_Spawned[loop] = spawned.GetComponent<IGuiListable>();
                    m_Spawned[loop].Setup(this, m_Listables[loop].MainText, m_Listables[loop].BasicType);
                }
            }
        }

        public virtual void ListableCallback( int type )
        {
            // do nothing
        }

        public virtual void RootClose ()
        {
            // do nothing
        }
    }
}