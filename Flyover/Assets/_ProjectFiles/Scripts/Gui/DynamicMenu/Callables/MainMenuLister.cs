﻿using UnityEngine;

namespace GGJ17.Gui.DynamicMenu.Callables
{
    /// <summary>
    /// Basic addition to a GuiLister to work with a MainMenuRoot
    /// </summary>
    public class MainMenuLister : GuiLister
    {
        MainMenu m_ParentRoot;

        public override void RootOpen (MenuRoot caller)
        {
            base.RootOpen(caller);

            m_ParentRoot = caller as MainMenu;
            if(m_ParentRoot == null)
            {
                Debug.LogWarning("MainMenuLister: RootOpen: MenuRoot caller is not a MainMenu type, this lister will not function", gameObject);
            }
        }

        public override void ListableCallback (int type)
        {
            if (!m_ParentRoot)
                return;

            switch (type)
            {
                case 0:
                    m_ParentRoot.OpenGame();
                    break;
                case 1:
                    m_ParentRoot.OpenOptions();
                    break;
                case 2:
                    m_ParentRoot.OpenCredits();
                    break;
                case 3:
                    m_ParentRoot.ExitApp();
                    break;
                default:
                    Debug.LogWarning("MainMenuLister[" + name + "]: Clicked: has an invalid button type, should be 0-3");
                    break;
            }
        }
    }
}
