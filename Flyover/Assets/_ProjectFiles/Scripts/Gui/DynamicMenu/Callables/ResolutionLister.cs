﻿using System;

using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.DynamicMenu.Callables
{
    /// <summary>
    /// Listers that spawns buttons for switching resolutions
    /// </summary>
    public class ResolutionLister : GuiLister
    {
        [SerializeField] private Text               m_CurrentResText;
        [SerializeField] private bool               m_SetFullscreen = true;

        [Header("Resolutions")]
        [SerializeField] private Resolution[]       m_Resolutions;
        
        protected IGuiListable[]                    m_Spawned;

        private const string        M_RESOLUTION_FORMAT = "w{0} x h{1}";

        public override void RootOpen (MenuRoot caller)
        {
            m_CurrentResText.text = string.Format("Resolution: " + M_RESOLUTION_FORMAT, Screen.width, Screen.height);

            if (m_Spawned == null || m_Spawned.Length != m_Resolutions.Length)
            {
                int spawnCount = m_Resolutions.Length;
                m_Spawned = new IGuiListable[spawnCount];

                for (int loop = 0; loop < spawnCount; loop++)
                {
                    GameObject spawned = Instantiate(m_Listables[0].Prefab);
                    Transform spawnTransf = spawned.transform;

                    spawnTransf.SetParent(m_SpawnRoot);
                    spawnTransf.localScale = Vector3.one;

                    m_Spawned[loop] = spawned.GetComponent<IGuiListable>();
                    m_Spawned[loop].Setup(this, m_Resolutions[loop].ToString(), loop);
                }
            }
        }

        public override void ListableCallback (int type)
        {
            SetResolution(m_Resolutions[type]);
        }

        private void SetResolution(Resolution res)
        {
            Screen.SetResolution(res.Width, res.Height, m_SetFullscreen);
            m_CurrentResText.text = string.Format("Resolution: " + M_RESOLUTION_FORMAT, Screen.width, Screen.height);
        }

        [Serializable]
        public struct Resolution
        {
            public int Width;
            public int Height;

            public override string ToString ()
            {
                return string.Format(M_RESOLUTION_FORMAT, Width, Height);
            }
        }
    }
}
