using UnityEngine;

namespace GGJ17.Gui.DynamicMenu
{
    /// <summary>
    /// Base class for a Root of various GUI in the main menu
    /// </summary>
    public class MenuRoot : MonoBehaviour
    {
        protected IRootCallable[]       p_Startables = new IRootCallable[0];

        [SerializeField] [Tooltip("Whether to allow re-searching for Startables every time this root is opened")]
        private bool                    m_CanReFetchStartables = false;

        private void Start()
        {
            FetchCallables();
            InheritedStart();
        }
        // to ensure startables are found and checked use this as Start for inherited MenuRoots
        protected virtual void InheritedStart () { }

        private void FetchCallables()
        {
            if (p_Startables.Length > 0
                && !m_CanReFetchStartables)
            {
                return;
            }

            p_Startables = GetComponentsInChildren<IRootCallable>();
            if (Application.isEditor && p_Startables.Length < 1)
            {
                Debug.LogWarning("MenuRoot[" + name + "]: Start(): has found no Startables in it's children", gameObject);
            }
        }

        
        public virtual void OpenRoot ()
        {
            FetchCallables();
            gameObject.SetActive(true);

            for(int loop = 0; loop < p_Startables.Length; loop++)
            {
                if (p_Startables[loop] == null)
                {
                    Debug.LogWarning("MenuRoot[" + name + "]: OpenRoot(): p_Startables[" + loop + "] is null, skipping", gameObject);
                    continue;
                }

                p_Startables[loop].RootOpen(this);
            }
        }

        public virtual void CloseRoot ()
        {
            for (int loop = 0; loop < p_Startables.Length; loop++)
            {
                if (p_Startables[loop] == null)
                {
                    Debug.LogWarning("MenuRoot[" + name + "]: CloseRoot(): p_Startables[" + loop + "] is null, skipping", gameObject);
                    continue;
                }

                p_Startables[loop].RootClose();
            }

            gameObject.SetActive(false);
        }

        public void OpenOtherRoot(MenuRoot otherRoot)
        {
            CloseRoot();
            otherRoot.OpenRoot();
        }
    }
}