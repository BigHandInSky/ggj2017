﻿using GGJ17.Gui.DynamicMenu.Callables;

using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.DynamicMenu.Listables
{
    /// <summary>
    /// Script for buttons that can be spawned and controlled by a Lister
    /// </summary>
    public class ButtonListable : MonoBehaviour, IGuiListable
    {
        [SerializeField] protected Text       m_MainText;
        [SerializeField] protected Button     m_ButtonComponent;

        protected GuiLister                 m_Lister;
        protected int                       m_ButtonType = -1;
        
        public virtual void Setup (GuiLister lister, string text, int type)
        {
            if(m_ButtonType < 0)
                m_ButtonComponent.onClick.AddListener(Clicked);

            m_Lister = lister;
            m_MainText.text = text;
            m_ButtonType = type;
        }
                
        protected virtual void Clicked()
        {
            m_Lister.ListableCallback(m_ButtonType);
        }
    }
}
