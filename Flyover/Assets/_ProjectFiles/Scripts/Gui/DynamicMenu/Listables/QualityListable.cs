﻿using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.DynamicMenu.Listables
{
    /// <summary>
    /// Listable button that increments between Unity Quality settings
    /// </summary>
    public class QualityListable : ButtonListable
    {
        [SerializeField] private Text   m_QualityText;
        [SerializeField] private bool   m_AllowExpensiveChanges = true;
        private int                     m_TotalLevels = -1;

        private const string M_QUALITY_VALUE_FORMAT = "Level {0}/{1}";

        public override void Setup (Callables.GuiLister lister, string text, int type)
        {
            base.Setup(lister, text, type);

            m_ButtonType = QualitySettings.GetQualityLevel();
            m_QualityText.text = QualitySettings.names[m_ButtonType];
            m_TotalLevels = QualitySettings.names.Length;
            m_MainText.text = string.Format(M_QUALITY_VALUE_FORMAT, m_ButtonType + 1, m_TotalLevels);
        }

        protected override void Clicked ()
        {
            base.Clicked();

            m_ButtonType++;
            if (m_ButtonType >= m_TotalLevels)
                m_ButtonType = 0;

            m_QualityText.text = QualitySettings.names[m_ButtonType];
            QualitySettings.SetQualityLevel(m_ButtonType, m_AllowExpensiveChanges);
            m_MainText.text = string.Format(M_QUALITY_VALUE_FORMAT, m_ButtonType + 1, m_TotalLevels);
        }
    }
}
