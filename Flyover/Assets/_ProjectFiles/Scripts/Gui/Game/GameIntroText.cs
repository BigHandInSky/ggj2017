﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Gui.Game
{
    public class GameIntroText : MonoBehaviour
    {
        [SerializeField] private CanvasGroup m_Group;

        IEnumerator Start()
        {            
            while(!Input.anyKey)
            {
                yield return null;           
            }

            Debug.Log("input detected, fading");

            m_Group.alpha = 1.0f;
            m_Group.interactable = false;
            m_Group.blocksRaycasts = false;
            m_Group.ignoreParentGroups = true;

            yield return new WaitForSeconds(3.0f);

            float lerp = 0.0f;
            while(lerp < 1.0f)
            {
                yield return null;
                lerp += Time.deltaTime / 5.0f;

                m_Group.alpha = 1 - lerp;
            }

            m_Group.alpha = 0.0f;
        }
    }
}
