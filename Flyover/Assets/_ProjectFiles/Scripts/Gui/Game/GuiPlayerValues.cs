﻿using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.Game
{
    public class GuiPlayerValues : MonoBehaviour
    {
        [Header("Score")]
        [SerializeField] private Text           m_Score;
        [SerializeField] private Text           m_Multiplier;
        [SerializeField] private Slider         m_MultiRamp;

        [Header("Lives")]
        [SerializeField] private RectTransform  m_Area;
        [SerializeField] private Sprite         m_NormalSprite;
        [SerializeField] private Sprite         m_UsedSprite;

        private Image[] m_LifeIcons = new Image[Common.NUM_OF_LIVES];
        
        public void EndOfLife()
        {
            if(m_LifeIcons[0] == null)
            {
                for (int loop = 0; loop < Common.NUM_OF_LIVES; loop++)
                {
                    GameObject clone = new GameObject("life_" + loop);
                    clone.transform.SetParent(m_Area);
                    clone.transform.localScale = Vector3.one;

                    m_LifeIcons[loop] = clone.AddComponent<Image>();
                    m_LifeIcons[loop].sprite = m_NormalSprite;
                    m_LifeIcons[loop].preserveAspect = true;
                }
            }

            for (int loop = 0; loop < Common.NUM_OF_LIVES; loop++)
            {
                m_LifeIcons[loop].sprite = (loop < Common.CurrentLife) ? m_UsedSprite : m_NormalSprite;
            }
        }        

        void Update()
        {
            m_Score.text = Common.ScoreString;
            m_Multiplier.text = Common.MultiString;
            m_MultiRamp.value = Common.MultiRamp;
        }
    }
}