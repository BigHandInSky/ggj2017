﻿using UnityEngine;

namespace GGJ17.Gui.Game
{
    public class GuiScoreList : MonoBehaviour
    {
        [SerializeField] private GameObject     m_Prefab;
        [SerializeField] private RectTransform  m_Area;
        
        [Header("Colours")]
        [SerializeField] private Color m_BestRun = Color.white;
        [SerializeField] private Color m_WorstRun = Color.red;
        [SerializeField] private Color m_HideColor = Color.clear;

        private GuiScoreDisplayer[] m_Displayers = new GuiScoreDisplayer[Common.NUM_OF_LIVES];

        void Start()
        {
            for (int loop = 0; loop < Common.NUM_OF_LIVES; loop++)
            {
                GameObject clone = Instantiate(m_Prefab);
                clone.transform.SetParent(m_Area);
                clone.transform.localScale = Vector3.one;
                
                m_Displayers[loop] = clone.GetComponent<GuiScoreDisplayer>();
                m_Displayers[loop].SetColors(m_HideColor);
            }
        }
        
        public void UpdateList()
        {
            for (int loop = 0; loop < Common.NUM_OF_LIVES; loop++)
            {
                if(!m_Displayers[loop])
                {
                    Debug.LogError("null scorelist entry at " + loop);
                }

                if(loop < Common.CurrentLife)
                {
                    m_Displayers[loop].SetColors(m_BestRun);
                    m_Displayers[loop].Setup(Common.Scores[loop].FinalScore, loop + 1);
                }
                else
                {
                    m_Displayers[loop].SetColors(m_HideColor);
                }
            }
        }
    }
}