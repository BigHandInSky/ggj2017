﻿using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.Game
{
    public class GuiScoreDisplayer : MonoBehaviour
    {
        [SerializeField] private Text m_ScoreText;
        [SerializeField] private Text m_LifeText;

        public void Setup(int score, int life)
        {
            m_ScoreText.text = score.ToString();
            m_LifeText.text = life.ToString();
        }

        public void SetColors(Color setCol)
        {
            m_ScoreText.color = setCol;
            m_LifeText.color = setCol;
        }
    }
}