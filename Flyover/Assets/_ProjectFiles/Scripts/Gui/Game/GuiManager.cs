﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Gui.Game
{
    public class GuiManager : MonoBehaviour
    {
        private static GuiManager m_Instance;
        public static GuiManager Instance
        {
            get
            {
                if (!m_Instance)
                    m_Instance = FindObjectOfType<GuiManager>();

                return m_Instance;
            }
        }

        [SerializeField] private GuiPlayerValues    m_PlayerValues;
        [SerializeField] private GuiScoreList       m_ScoreList;
        [SerializeField] private GuiRunEnd          m_EndScreen;
        [SerializeField] private GuiGameEnd         m_FinalScreen;
        [SerializeField] private GameObject         m_ButtonsRoot;
        [SerializeField] private CanvasGroup        m_Fill;
        [SerializeField] private AudioSource        m_Source;
        [Space(5)]
        [SerializeField]
        private Utilities.GuiSchwp.SchwpGroup       m_SchwpGroup;
        [SerializeField]
        private Utilities.GuiSchwp.Schwpper         m_EndPanelSchwpper;

        void Start()
        {
            m_EndScreen.Close();
            m_FinalScreen.Close();

            m_Fill.alpha = 0.0f;
            m_Fill.blocksRaycasts = false;            

            m_PlayerValues.EndOfLife();
            m_PlayerValues.gameObject.SetActive(true);

            m_ScoreList.UpdateList();
            m_ScoreList.gameObject.SetActive(true);

            m_ButtonsRoot.SetActive(true);

            m_SchwpGroup.TriggerSchwps(true);
        }

        void Update()
        {
            if(Input.GetKey(KeyCode.Escape))
            {
                TriggerQuit();
            }
        }

        public void PlayAudio()
        {
            m_Source.Play();
        }

        public void EndOfLife()
        {
            m_SchwpGroup.TriggerSchwps(false);

            StartCoroutine(FadeFill(0.0f, 0.5f));
            m_EndScreen.Open(Common.LastScoreValue);
            m_EndPanelSchwpper.Trigger(true);

            m_PlayerValues.EndOfLife();

            m_ScoreList.UpdateList();
        }

        public void EndOfGame()
        {
            m_SchwpGroup.TriggerSchwps(false);

            StartCoroutine(FadeFill(0.0f, 0.5f));
            m_FinalScreen.Open(Common.ScoresOrdered[Common.NUM_OF_LIVES - 1]);
            m_EndPanelSchwpper.Trigger(true);
        }

        // called by buttons
        public void TriggerRestart()
        {
            m_EndPanelSchwpper.Trigger(false);
            m_EndScreen.Close();
            m_FinalScreen.Close();
            StartCoroutine(FadeFill(0.5f, 0.0f));

            Common.Restart();

            Player.PlayerController.Instance.Restart();
            World.WorldManager.Instance.Restart();

            m_PlayerValues.EndOfLife();
            m_ScoreList.UpdateList();

            m_SchwpGroup.TriggerSchwps(true);
        }
        public void TriggerNextLife()
        {
            m_EndPanelSchwpper.Trigger(false);
            StartCoroutine(FadeFill(0.5f, 0.0f));
            m_EndScreen.Close();

            Player.PlayerController.Instance.Restart();
            World.WorldManager.Instance.NewLife();

            m_PlayerValues.EndOfLife();
            m_ScoreList.UpdateList();

            m_SchwpGroup.TriggerSchwps(true);
        }
        public void TriggerMenu()
        {
            Debug.LogError("TriggerMenu not coded");
        }
        public void TriggerQuit()
        {
            m_EndPanelSchwpper.Trigger(false);
            Application.Quit();
        }

        private IEnumerator FadeFill(float startAlpha, float gotoAlpha)
        {
            float lerp = 0.0f;
            while(lerp < 1.0f)
            {
                yield return null;
                lerp += Time.deltaTime / 0.25f;

                m_Fill.alpha = Mathf.Lerp(startAlpha, gotoAlpha, lerp);
            }
        }
    }
}