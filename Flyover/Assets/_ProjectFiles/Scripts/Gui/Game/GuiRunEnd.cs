﻿using UnityEngine;
using UnityEngine.UI;

namespace GGJ17.Gui.Game
{
    public class GuiRunEnd : MonoBehaviour
    {
        [SerializeField] private GameObject m_Line;
        [SerializeField] private GameObject[] m_EnableButtons;
        [Space(5)]
        [SerializeField] private Text m_ScoreText;
        [SerializeField] private Text m_MultiText;
        [SerializeField] private Text m_FinalText;
        [SerializeField] private Text m_LivesText;

        public void Open(ScoreValue data)
        {
            m_EnableButtons[0].SetActive(false);
            m_EnableButtons[1].SetActive(true);
            m_EnableButtons[2].SetActive(true);

            transform.parent.gameObject.SetActive(true);
            gameObject.SetActive(true);
            m_Line.SetActive(true);

            m_ScoreText.text = data.RunScore.ToString(Common.FORM_SCORE);
            m_MultiText.text = data.Multiplier.ToString(Common.FORM_MULTI);
            m_FinalText.text = data.FinalScore.ToString(Common.FORM_SCORE);
            m_LivesText.text = (Common.LivesLeft + 1).ToString() + " Lives Left";
        }

        public void Close()
        {
            transform.parent.gameObject.SetActive(false);
            gameObject.SetActive(false);
            m_Line.SetActive(false);
        }
    }
}