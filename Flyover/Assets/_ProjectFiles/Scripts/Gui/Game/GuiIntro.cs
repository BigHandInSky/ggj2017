﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GGJ17.Player;
using Utilities.GuiSchwp;
using System;

namespace GGJ17.Gui.Game
{
    public class GuiIntro : MonoBehaviour, ISchwpCallbackable
    {
        [SerializeField] private GameObject     m_IntroRoot;
        [SerializeField] private GameObject     m_LoadingRoot;
        [SerializeField] private Slider         m_LoadSlider;
        [SerializeField] private AudioSource    m_Source;
        [SerializeField] private Text           m_ControlText;
        [Space(5)]
        [SerializeField] private Schwpper       m_LoadBarSchwpper;
        void Start()
        {
            Common.ControlMethod = ControlType.WASD;
            m_ControlText.text = "WASD or Arrows";

            m_IntroRoot.SetActive(true);
            m_LoadingRoot.SetActive(false);
            m_LoadSlider.value = 0.0f;
        }

        public void PlayAudio()
        {
            m_Source.Play();
        }

        public void Play()
        {
            m_IntroRoot.SetActive(false);
            m_LoadingRoot.SetActive(true);

            m_LoadBarSchwpper.Trigger(true, this);
        }

        public void Callback (MonoBehaviour caller)
        {
            StartCoroutine(Loading());
        }

        System.Collections.IEnumerator Loading ()
        {
            yield return null;

            AsyncOperation loadOp = SceneManager.LoadSceneAsync(1);

            while(!loadOp.isDone)
            {
                yield return null;
                m_LoadSlider.value = loadOp.progress;
            }

            SceneManager.UnloadSceneAsync(0);
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void SwitchControlType()
        {
            if(Common.ControlMethod == ControlType.WASD)
            {
                Common.ControlMethod = ControlType.Mouse;
                m_ControlText.text = "Mouse Aim";
            }
            else
            {
                Common.ControlMethod = ControlType.WASD;
                m_ControlText.text = "WASD or Arrows";
            }
        }
    }
}
