﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Abjects
{
    [RequireComponent(typeof(Collider))]
    public abstract class Abject : MonoBehaviour
    {
        private Transform                       m_RootTransf;
        protected AbjectType                    m_Type;

        [Header("Base:Visual")]
        [SerializeField] private MeshRenderer   m_Renderer;
        [SerializeField] private AudioSource    m_ActionSource;
        [SerializeField] private LineRenderer   m_AimLine;
        /*[SerializeField] private TrailRenderer  m_Trail;*/
        
        [Header("Base:Settings")]
        [SerializeField] private Color          m_NormalCol = Color.white;  // idle
        [SerializeField] private Color          m_ActionCol = Color.red;    // at height of action
        [SerializeField] private Color          m_EngageCol = Color.red;    // at height of action
        [Space(5)]
        [SerializeField] private float          m_BaseMoveSpeed = 25.0f;
        [SerializeField] private float          m_BaseActionWait = 1.0f;
        [SerializeField] private float          m_BaseActionLength = 1.0f;

        public AbjectType           GetAbjectType { get { return m_Type; } }

        protected Transform         RootTransf { get { return m_RootTransf; } }
        protected MeshRenderer      Renderer { get { return m_Renderer; } }
        protected AudioSource       ActionSource { get { return m_ActionSource; } }
        protected LineRenderer      AimLine { get { return m_AimLine; } }
        /*protected TrailRenderer     Trail { get { return m_Trail; } }*/

        protected Color             NormalCol { get { return m_NormalCol; } }
        protected Color             ActionCol { get { return m_ActionCol; } }
        protected Color             EngageCol { get { return m_EngageCol; } }

        protected float             BaseMoveSpeed { get { return m_BaseMoveSpeed; } }
        protected float             BaseActionWait { get { return m_BaseActionWait; } }
        protected float             BaseActionLength { get { return m_BaseActionLength; } }

        private IEnumerator Start ()
        {
            m_RootTransf = transform;
            m_Type = SetType();

            bool previousSpace = m_AimLine.useWorldSpace;
            Gradient aimGradient = m_AimLine.colorGradient;
            
            m_AimLine.useWorldSpace = true;
            m_AimLine.colorGradient = new Gradient();
            m_AimLine.SetPosition(0, m_RootTransf.position);
            m_AimLine.SetPosition(1, Player.PlayerMover.PlayerPosition);

            yield return StartCoroutine(Util.Reactions.WaitForReaction());
            
            m_AimLine.useWorldSpace = previousSpace;
            m_AimLine.colorGradient = aimGradient;
            m_AimLine.SetPosition(0, Vector3.zero);
            m_AimLine.SetPosition(1, Vector3.zero);

            StartCoroutine(Behaviour());
        }

        public virtual void StartingVectors(Vector3 distance, Vector3 euler)
        {
            // do nothing in base
        }        

        protected abstract AbjectType SetType();
        protected abstract IEnumerator Behaviour();
        
        public void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.CompareTag(Common.TAG_PLAYER))
            {
                Debug.Log("Abject[" + name + "]: OnCollisionEnter(" + collision + "): hit player");
                // collision handled on player, do some particle stuff here
            }
            else if (collision.gameObject.CompareTag(Common.TAG_OBSTCL))
            {
                Debug.Log("Abject[" + name + "]: OnCollisionEnter(" + collision + "): hit obstacle");
                Destroyed();
            }
        }

        public void EndOfLifeKill()
        {
            Destroy(gameObject);
        }
        private void Destroyed()
        {
            Destroy(gameObject);
        }
    }

    public enum AbjectType
    {
        Square,
        Pyramid,
        Sphere,
        Cylinder
    }
}