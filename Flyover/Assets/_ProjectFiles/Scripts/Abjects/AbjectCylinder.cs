﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GGJ17.World;

namespace GGJ17.Abjects
{
    public class AbjectCylinder : Abject
    {/*
        [Header("Abject:Cylinder")]
        [SerializeField] private LineRenderer   m_AimLine;*/

        private List<Platform>                  m_Platforms;

        protected override AbjectType SetType()
        {
            return AbjectType.Cylinder;
        }
        protected override IEnumerator Behaviour()
        {
            float actionWait = 0.0f;
            float actionTime = BaseActionLength;

            m_Platforms = FindObjectsOfType<Platform>().ToList();
            for(int loop = 0; loop < m_Platforms.Count; loop++)
            {
                if(m_Platforms[loop].IsStartingPlatform)
                {
                    m_Platforms.RemoveAt(loop);
                    break;
                }
            }

            Platform lastPlatform = null;
            Platform currentPlatform = FindNewPlatform(null);

            while (gameObject)
            {
                yield return null;

                actionWait -= Time.deltaTime;
                float actionLerpMod = actionWait / BaseActionWait;
                
                AimLine.SetPosition(0, RootTransf.position);
                AimLine.SetPosition(1, Vector3.Lerp(currentPlatform.WorldPos, RootTransf.position, actionLerpMod));

                Renderer.material.color = Color.Lerp(ActionCol, NormalCol, actionLerpMod);  

                if (actionWait <= 0.0f)
                {
                    ActionSource.Play();
                    actionWait = BaseActionWait;
                    actionTime = BaseActionLength;
                    Renderer.material.color = EngageCol;

                    RootTransf.LookAt(currentPlatform.WorldPos);

                    while (actionTime > 0.0f)
                    {
                        yield return null;
                        actionTime -= Time.deltaTime;

                        RootTransf.position = Vector3.Lerp(
                            currentPlatform.WorldPos,
                            RootTransf.position, 
                            actionTime / BaseActionLength);

                        AimLine.SetPosition(0, RootTransf.position);
                    }

                    lastPlatform = currentPlatform;
                    currentPlatform = FindNewPlatform(lastPlatform);

                    Renderer.material.color = NormalCol;
                }
              
                RootTransf.LookAt(Player.PlayerMover.PlayerPosition);

                RootTransf.position = Vector3.Lerp(
                    lastPlatform.WorldPos,
                    RootTransf.position,
                    Time.deltaTime);
            }
        }

        protected Platform FindNewPlatform (Platform current)
        {
            List<Platform> platforms = m_Platforms;

            if (current != null)
            {
                platforms.Remove(current);
                current.HasCylinder = false;
            }

            int nextPlatform = Random.Range(0, platforms.Count);
            while(platforms[nextPlatform].HasCylinder)
            {
                //Debug.Log(nextPlatform);
                nextPlatform++;

                if (nextPlatform >= platforms.Count)
                    nextPlatform = 0;
            }

            platforms[nextPlatform].HasCylinder = true;

            return platforms[nextPlatform];
        }
    }
}
