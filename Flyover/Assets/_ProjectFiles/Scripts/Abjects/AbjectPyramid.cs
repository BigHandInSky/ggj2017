﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Abjects
{
    public class AbjectPyramid : Abject
    {
        [Header("Abject:Pyramid")]
        [SerializeField] private float      m_ActionSpeed = 5.0f;
        [SerializeField] private int        m_AimLineDistNorm = 3;
        [SerializeField] private int        m_AimLineDistActn = 6;

        protected override AbjectType SetType()
        {
            return AbjectType.Pyramid;
        }
        protected override IEnumerator Behaviour()
        {
            AimLine.SetPosition(1, new Vector3(0,0, m_AimLineDistNorm));

            float actionWait = BaseActionWait;
            float actionTime = BaseActionLength;
            float moveSpeed = BaseMoveSpeed;

            while (gameObject)
            {
                yield return null;
                
                actionWait -= Time.deltaTime;
                float actionLerpMod = actionWait / BaseActionWait;

                moveSpeed = Mathf.Lerp(BaseMoveSpeed, 0.1f, actionLerpMod);
                Renderer.material.color = Color.Lerp(ActionCol, NormalCol, actionLerpMod);

                RootTransf.localPosition += RootTransf.forward * moveSpeed * Time.deltaTime;
                //RootTransf.LookAt(Player.PlayerMover.PlayerPosition);
                                
                Vector3 lookDirection = Player.PlayerMover.PlayerPosition - RootTransf.position;
                RootTransf.localRotation = Quaternion.Slerp(
                    RootTransf.localRotation,
                    Quaternion.LookRotation(lookDirection.normalized),
                    Time.deltaTime * 2.0f);

                if (actionWait <= 0.0f)
                {
                    ActionSource.Play();
                    
                    actionWait = BaseActionWait;
                    actionTime = BaseActionLength;
                    Renderer.material.color = EngageCol;

                    AimLine.SetPosition(1, new Vector3(0, 0, m_AimLineDistActn));
                    yield return StartCoroutine(Util.Reactions.WaitForReaction());
                    
                    while (actionTime > 0.0f)
                    {
                        yield return null;
                        actionTime -= Time.deltaTime;

                        RootTransf.localPosition += RootTransf.forward * m_ActionSpeed * Time.deltaTime;
                        //RootTransf.LookAt(Player.PlayerMover.PlayerPosition);
                    }

                    Renderer.material.color = NormalCol;
                    AimLine.SetPosition(1, new Vector3(0, 0, m_AimLineDistNorm));
                }
            }
        }
    }
}
