﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Abjects
{
    public class AbjectSquare : Abject
    {
        [Header("Abject:Square")]
        [SerializeField] private float m_ActionSpeed = 5.0f;
        [SerializeField] private float m_AimForwDistance = 1.0f;
        [SerializeField] private float m_Aim90Distance = 1.0f;
        [SerializeField] private float m_Aim45Distance = 2.5f;

        protected override AbjectType SetType()
        {
            return AbjectType.Square;
        }
        protected override IEnumerator Behaviour()
        {
            float actionWait = BaseActionWait;
            AimLine.SetPosition(1, Vector3.forward * m_AimForwDistance);

            while (gameObject)
            {
                yield return null;

                actionWait -= Time.deltaTime;
                float actionLerpMod = actionWait / BaseActionWait;
                
                Renderer.material.color = Color.Lerp(ActionCol, NormalCol, actionLerpMod);

                RootTransf.localPosition += RootTransf.forward * BaseMoveSpeed * Time.deltaTime;

                Vector3 lookDirection = Player.PlayerMover.PlayerPosition - RootTransf.position;
                RootTransf.localRotation = Quaternion.Slerp(
                    RootTransf.localRotation,
                    Quaternion.LookRotation(lookDirection.normalized),
                    Time.deltaTime * 1.2f);

                if (actionWait <= 0.0f)
                {
                    actionWait = BaseActionWait;
                    Renderer.material.color = EngageCol;

                    bool use45 = Random.Range(0, 2) == 0;
                    bool left = Random.Range(0, 2) == 0;

                    SetAimLine(use45, left);

                    yield return PerformAction(
                        (use45) ? BaseActionLength * 0.5f : BaseActionLength,
                        (use45) ? 45 : 90,
                        left);

                    if(use45)
                    {
                        SetAimLine(use45, left);

                        yield return PerformAction(
                            BaseActionLength * 0.5f,
                            45,
                            left);
                    }

                    AimLine.SetPosition(1, Vector3.forward * m_AimForwDistance);
                    Renderer.material.color = NormalCol;
                }
            }
        }

        private IEnumerator PerformAction(float length, float rotation, bool left)
        {
            ActionSource.Play();

            yield return StartCoroutine(Util.Reactions.WaitForReaction());
            AimLine.SetPosition(1, Vector3.forward * m_AimForwDistance);

            Vector3 euler = Vector3.zero;
            euler.y = rotation;

            if (left)
            {
                RootTransf.localRotation = Quaternion.Euler(RootTransf.localEulerAngles - euler);
            }
            else
            {
                RootTransf.localRotation = Quaternion.Euler(RootTransf.localEulerAngles + euler);
            }
            
            while (length > 0.0f)
            {
                yield return null;
                length -= Time.deltaTime;

                RootTransf.localPosition += RootTransf.forward * m_ActionSpeed * Time.deltaTime;
            }
        }

        private void SetAimLine(bool use45, bool left)
        {
            // default to right, as it's positive
            Vector3 aimDirection = Vector3.zero;
            aimDirection.x = ( use45 ) ? m_Aim45Distance : m_Aim90Distance;
            aimDirection.z = ( use45 ) ? m_Aim45Distance : 0;

            if (left)
                aimDirection.x = -aimDirection.x;

            AimLine.SetPosition( 1, aimDirection);
        }
    }
}
