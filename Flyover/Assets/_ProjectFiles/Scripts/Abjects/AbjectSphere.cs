﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Abjects
{
    public class AbjectSphere : Abject
    {
        [Header("Abject:Sphere")]
        //[SerializeField] private LineRenderer m_AimLine;
        [SerializeField] private float m_MinRadius = 5.0f;
        [SerializeField] private float m_MaxRadius = 10.0f;
        [Space(5)]
        [SerializeField] private float m_MinRadiusSpeed = 5.0f;
        [SerializeField] private float m_MaxRadiusSpeed = 25.0f;

        private Vector3 m_Distance;
        private Vector3 m_Euler;

        protected override AbjectType SetType()
        {
            return AbjectType.Sphere;
        }

        public override void StartingVectors(Vector3 distance, Vector3 euler)
        {
            m_Distance = distance;
            m_Euler = euler;
        }

        protected override IEnumerator Behaviour()
        {
            float actionWait = BaseActionWait;            
            float moveSpeed = BaseMoveSpeed;
            
            float gotoRadius = Random.Range(m_MinRadius, m_MaxRadius);
            Vector3 newPosition = Quaternion.Euler(m_Euler) * m_Distance;

            float maxMoveSpeed = (((gotoRadius - m_MinRadius) * (m_MaxRadiusSpeed - m_MinRadiusSpeed)) / (m_MaxRadius - m_MinRadius)) + m_MinRadiusSpeed;

            while (gameObject)
            {
                yield return null;

                actionWait -= Time.deltaTime;
                float actionLerpMod = actionWait / BaseActionWait;

                moveSpeed = Mathf.Lerp(maxMoveSpeed, 0.1f, actionLerpMod);
                Renderer.material.color = Color.Lerp(ActionCol, NormalCol, actionLerpMod);

                AimLine.SetPosition(1, new Vector3(0, 0, gotoRadius - m_Distance.z));

                m_Distance.z = Mathf.MoveTowards(m_Distance.z, gotoRadius, Time.deltaTime);
                newPosition = Quaternion.Euler(m_Euler) * m_Distance;
                RootTransf.localPosition = newPosition;

                m_Euler.y += moveSpeed * Time.deltaTime;
                RootTransf.localEulerAngles = m_Euler;

                //RootTransf.LookAt(Player.PlayerMover.PlayerPosition);
                
                if (actionWait <= 0.0f)
                {
                    ActionSource.Play();
                    actionWait = BaseActionWait;
                    Renderer.material.color = EngageCol;

                    gotoRadius = Random.Range(m_MinRadius, m_MaxRadius);
                    maxMoveSpeed = (((gotoRadius - m_MinRadius) * (m_MaxRadiusSpeed - m_MinRadiusSpeed)) / (m_MaxRadius - m_MinRadius)) + m_MinRadiusSpeed;
                }
            }
        }
    }
}
