﻿using UnityEngine;

namespace GGJ17.Util
{
    public class CamMover : MonoBehaviour
    {
        private Transform m_SelfTransf;

        private Vector3 m_Start;
        private Vector3 m_Goto;

        void Start()
        {
            m_SelfTransf = transform;
        }

        void Update()
        {
            m_Start = m_SelfTransf.position;
            m_Goto = Player.PlayerMover.PlayerPosition;

            m_SelfTransf.position = Vector3.Lerp(m_Start, m_Goto, Time.deltaTime);
        }
    }
}
