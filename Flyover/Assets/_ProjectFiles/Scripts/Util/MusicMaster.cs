﻿using System.Collections;
using UnityEngine;

namespace GGJ17.Util
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicMaster : MonoBehaviour
    {
        [SerializeField] private AudioClip[]    m_Music = new AudioClip[0];
        [SerializeField] private string[]       m_MusicTitles = new string[0];
        [SerializeField]
        private UnityEngine.UI.Text             m_MusicName;
        [SerializeField]
        private UnityEngine.UI.Slider           m_MusicProgress;

        private AudioSource                     m_Source;

        private int                             m_CurrentClip = 0;

        private float                           m_CurrentClipProgress = 0.0f;
        private float                           m_CurrentClipLength = 0.0f;

        private bool                            m_Restart = false;
        private bool                            m_Skip = false;        

        public static float Intensity { get; private set; }

        private const float INTENSITY_MOD = 1.0F;

        IEnumerator Start()
        {
            yield return null;

            m_Source = GetComponent<AudioSource>();
            m_Restart = false;
            m_Skip = false;

            if (m_Music != null && m_Music.Length > 0)
            {
                StartCoroutine(HandleMusic());
                StartCoroutine(HandleIntensity());
            }
        }

        IEnumerator HandleMusic()
        {
            m_CurrentClip = Random.Range(0, m_Music.Length);
            m_Source.Stop();
            m_Source.volume = 0;

            while (gameObject)
            {
                yield return null;

                m_Source.clip = m_Music[m_CurrentClip];
                m_MusicName.text = m_MusicTitles[m_CurrentClip];

                m_CurrentClipProgress = 0.0f;
                m_CurrentClipLength = m_Source.clip.length;
                m_MusicProgress.maxValue = m_CurrentClipLength;

                m_Source.Play();

                float lerp = 0;
                while (lerp < 1.0f)
                {
                    yield return null;
                    lerp += Time.deltaTime / 5.0f;
                    m_Source.volume = lerp;

                    Color col = m_MusicName.color;
                    col.a = 1.0f - lerp;
                    m_MusicName.color = col;

                    m_MusicProgress.value += Time.deltaTime / 5.0f;

                    if (m_Skip)
                    {
                        break;
                    }
                }

                float waitLength = m_Music[m_CurrentClip].length - 5.0f;
                while( waitLength > 0.0f )
                {
                    yield return null;
                    waitLength -= Time.deltaTime;                    
                    m_MusicProgress.value = m_CurrentClipLength - waitLength;

                    if (m_Restart)
                    {
                        m_Restart = false;
                        if (m_MusicProgress.value < 7)
                        {
                            continue;
                        }

                        waitLength = m_Music[m_CurrentClip].length;
                        m_Source.time = 0.0f;
                    }

                    if (m_Skip)
                    {
                        break;
                    }
                }

                while(lerp < 1.0f)
                {
                    yield return null;
                    lerp += Time.deltaTime / 5.0f;
                    m_Source.volume = 1.0f - lerp;
                    
                    Color col = m_MusicName.color;
                    col.a = lerp;
                    m_MusicName.color = col;

                    m_MusicProgress.value += Time.deltaTime / 5.0f;

                    if (m_Skip)
                    {
                        m_Skip = false;
                        break;
                    }
                }

                int nextClip = Random.Range(0, m_Music.Length);
                if(nextClip == m_CurrentClip + 1)
                {
                    nextClip++;
                }
                if (nextClip == m_CurrentClip - 1)
                {
                    nextClip--;
                }

                if (nextClip >= m_Music.Length)
                    nextClip = 0;
                if (nextClip <= -1)
                    nextClip = m_Music.Length;

                m_CurrentClip = nextClip;
            }
        }

        IEnumerator HandleIntensity()
        {
            while (gameObject)
            {
                yield return null;

                if (m_Source.isPlaying)
                {
                    //m_Intensity = m_Source.clip.frequency;
                    //Intensity = m_Source.clip.frequency * INTENSITY_MOD;

                }
            }
        }

        public void Next()
        {
            m_Skip = true;
        }
        public void Restart()
        {
            m_Restart = true;
        }
    }
}
