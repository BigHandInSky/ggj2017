﻿using System;
using System.Collections;
using UnityEngine;

namespace GGJ17.Util
{
    public static class Reactions
    {
        public const float          REACTION_LENGTH = 0.15F;

        public static IEnumerator WaitForReaction ()
        {
            float wait = REACTION_LENGTH;
            while (wait > 0)
            {
                wait -= Time.deltaTime;
                yield return null;
            }
        }
    }
}
