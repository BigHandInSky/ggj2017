﻿using System.Linq;
using GGJ17.Player;
using UnityEngine;

namespace GGJ17
{
    public static class Common
    {
        public const string         TAG_PLAYER = "Player";
        public const string         TAG_ABJECT = "Abject";
        public const string         TAG_OBSTCL = "Obstacle";
        public const string         TAG_PLATFM = "Platform";

        public const string         FORM_SCORE = "00000000";
        public const string         FORM_MULTI = "000";

        public const int            NUM_OF_LIVES = 6;
        public const int            FRAMES_PER_SCORE = 2;
        public const float          RAMP_INCREMENT_MOD = 0.25F;

        private static int          m_CurrentLife = 0;
        private static int          m_CurrentScore = 0;
        private static int          m_CurrentMulti = 1;
        private static float        m_MultiRamp = 0.0f;

        private static int          m_CurrentFrame = 0;

        private static ScoreValue[] m_Scores = new ScoreValue[NUM_OF_LIVES];

        public static ScoreValue[]  Scores { get { return m_Scores; } }
        public static ScoreValue[]  ScoresOrdered
        {
            get
            {/*
                ScoreValue[] returnValue = m_Scores.OrderBy(entry => entry.FinalScore).ToArray();
                for (int loop = 0; loop < returnValue.Length; loop++)
                    Debug.Log("scoresOrdered[" + loop + "]: index: " + returnValue[loop].LifeIndex + ", final: " + returnValue[loop].FinalScore);
                */
                return m_Scores.OrderBy(entry => entry.FinalScore).ToArray(); ;
            }
        }

        public static int           CurrentLife { get { return m_CurrentLife; } }
        public static int           LivesLeft { get { return NUM_OF_LIVES - (m_CurrentLife + 1); } }

        public static int           CurrentScore { get { return m_CurrentScore; } }
        public static int           CurrentMulti { get { return m_CurrentMulti; } }
        public static float         MultiRamp { get { return m_MultiRamp; } }

        public static ControlType   ControlMethod;

        public static ScoreValue    LastScoreValue
        {
            get
            {
                int index = m_CurrentLife - 1;
                if (index < 0)
                    index = NUM_OF_LIVES - 1;

                return m_Scores[index];
            }
        }
        public static ScoreValue    CurrentScoreValue { get { return m_Scores[m_CurrentLife]; } }

        public static string        ScoreString { get { return m_CurrentScore.ToString(FORM_SCORE); } }
        public static string        MultiString { get { return m_CurrentMulti.ToString(FORM_MULTI); } }

        public static void Restart()
        {
            m_CurrentLife = 0;
            m_CurrentScore = 0;
            m_CurrentMulti = 1;
            m_MultiRamp = 0.0f;

            m_Scores = new ScoreValue[NUM_OF_LIVES];
            for (int loop = 0; loop < NUM_OF_LIVES; loop++)
            {
                m_Scores[loop] = new ScoreValue(loop, 0, 0);
            }
        }

        public static void IncrementScore()
        {
            m_CurrentFrame++;
            if(m_CurrentFrame >= FRAMES_PER_SCORE)
            {
                m_CurrentFrame = 0;
                m_CurrentScore++;
            }
        }
        public static void IncrementMultiplier()
        {
            m_MultiRamp += Time.deltaTime * RAMP_INCREMENT_MOD;
            if (m_MultiRamp >= 1.0f)
            {
                m_MultiRamp = 0.0f;
                m_CurrentMulti++;
            }
        }

        public static void EndOfLife()
        {
            m_Scores[m_CurrentLife] = new ScoreValue(m_CurrentLife, m_CurrentScore, m_CurrentMulti);
            m_CurrentLife++;            

            m_CurrentScore = 0;
            m_CurrentMulti = 1;
            m_MultiRamp = 0.0f;

            if (m_CurrentLife >= NUM_OF_LIVES)
            {
                Debug.Log("end of game");
                m_CurrentLife = 0;
            }
        }

    }

    [System.Serializable]
    public class ScoreValue
    {
        public int LifeIndex = -1;
        public int RunScore = 0;
        public int Multiplier = 1;

        public int FinalScore = 0;

        public ScoreValue(int index, int score, int multi)
        {
            LifeIndex = index;
            RunScore = score;
            Multiplier = multi;

            FinalScore = RunScore * Multiplier;
        }

        public static bool operator <(ScoreValue a, ScoreValue b)
        {
            return (a.FinalScore < b.FinalScore);
        }
        public static bool operator >(ScoreValue a, ScoreValue b)
        {
            return (a.FinalScore > b.FinalScore);
        }
    }
}